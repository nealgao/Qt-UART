#include "wavewindow.h"
#include "ui_wavewindow.h"

#include "qcustomplot.h"
#include "wave.h"
#include "QTime"

//波形控件全局变量
//QCustomPlot *my_qcustomplot = new QCustomPlot;
extern float float_data;

WaveWindow::WaveWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WaveWindow)
{
    ui->setupUi(this);

    init_wave();
    //显示波形
    show_wave(&wave1);
}

WaveWindow::~WaveWindow()
{
    delete ui;
}
/********************************************
@function name:
@function detail:初始化波形
*******************************************/
void WaveWindow::init_wave(void)
{
    wave1.customplot = new QCustomPlot;
    wave1.line_num = 5;
    wave1.title = "测试波形";
    wave1.x_name = "x值";
    wave1.y_name = "y值";

}
/********************************************
@function name:
@function detail:显示波形
*******************************************/
void WaveWindow::show_wave(wave_info_struct* my_wave)
{
    static QTime time1(QTime::currentTime());
    double key = time1.elapsed()/1000.0;
    //设置轴可拉缩，鼠标可拉缩，轴可选，项目可选，其他物品可选
    my_wave->customplot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                     QCP::iSelectLegend | QCP::iSelectPlottables);
    my_wave->customplot->axisRect()->setupFullAxesBox();

    //设置分层
    //my_wave->customplot->plotLayout()->insertRow(0);
    //给层中添加表格
    //my_wave->customplot->plotLayout()->addElement()

    //设置坐标名称
    my_wave->customplot->xAxis->setLabel(my_wave->x_name);
    my_wave->customplot->yAxis->setLabel(my_wave->y_name);

    //设置用户是否可以通过点击波形表面选择可选择部分
    my_wave->customplot->legend->setSelectableParts(QCPLegend::spItems);
    my_wave->customplot->rescaleAxes();

    my_wave->customplot->addGraph();
    //添加一个曲线
    my_wave->customplot->graph(0);
    my_wave->customplot->graph(0)->setPen(QPen(QColor(40, 110, 255)));//蓝色

    //给曲线添加数据
    my_wave->customplot->graph(0)->addData(key, float_data);
    //显示当前范围
    //刷新
    my_wave->customplot->replot();

}

#-------------------------------------------------
#
# Project created by QtCreator 2017-10-20T20:14:16
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = TopBandUart
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    data_processing.cpp \
    qcustomplot.cpp \
    wavewindow.cpp

HEADERS  += mainwindow.h \
    data_processing.h \
    qcustomplot.h \
    wavewindow.h \
    wave.h

FORMS    += mainwindow.ui \
    wavewindow.ui
